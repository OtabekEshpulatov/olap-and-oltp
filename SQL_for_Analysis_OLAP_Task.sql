-- task1

SELECT p.prod_category,
       SUM(s.amount_sold * p.prod_list_price) AS total_sales_amount
FROM sh.sales s
         JOIN sh.products p ON s.prod_id = p.prod_id
WHERE s.time_id BETWEEN '1998-10-01' AND '2023-12-04'
GROUP BY p.prod_category
ORDER BY total_sales_amount DESC;


-- task2

SELECT c.country_id,
       AVG(s.quantity_sold) AS average_sales_quantity
FROM sh.sales s
         JOIN sh.customers c ON s.cust_id = c.cust_id
         JOIN sh.products p ON s.prod_id = p.prod_id
WHERE p.prod_name = '128MB Memory Card'
GROUP BY c.country_id
ORDER BY average_sales_quantity DESC;


-- task3

SELECT c.cust_first_name,
       c.cust_last_name,
       SUM(s.quantity_sold * p.prod_list_price) AS total_sales_amount
FROM sh.sales s
         JOIN sh.customers c ON s.cust_id = c.cust_id
         JOIN sh.products p ON p.prod_id=s.prod_id
     GROUP BY c.cust_id, c.cust_first_name, c.cust_last_name
ORDER BY total_sales_amount DESC
LIMIT 5;